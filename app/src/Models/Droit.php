<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Droit extends Model{
  	protected $table = "droit";
  	protected $primaryKey = 'id_droit';
  	public $timestamps=true;
  	public $incrementing = false;
}
