<?php

namespace App\Controllers;

use App\Models\Experience;
use App\Models\Video;
use App\Models\Reaction;
use App\Models\ReactionToParticipant as rtp;
use App\Models\Tag;
use App\Models\Comment;
use App\Models\Participant;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class BddController extends Controller{

    public function bdd(Request $request, Response $response, $args){
        $res= [];
        $loremIpsum = "Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut";
        $splitLoremIpsum = explode(" ", $loremIpsum);
        $createur = 0;
        foreach ($splitLoremIpsum as $key => $value) {
            $nExp = new Experience();
            $id_experience = uniqid();
            $nExp->id_experience = $id_experience;
            $nExp->title = $value.rand();
            $nExp->presentation = $loremIpsum;
            $nExp->id_video = "585a65ee46033";
            $nExp->id_state = 0;
            $nExp->explication = $loremIpsum;
            for ($i=0; $i < 4; $i++) { 
                $nReaction = new Reaction();
                $nReaction->id_reaction = uniqid();
                $nReaction->name = $value.'reaction'.$i.rand();
                $nReaction->id_experience=$id_experience;
                switch ($i) {
                    case 0:
                        $nReaction->color = 'blue';
                        break;
                    case 1:
                        $nReaction->color = 'red';
                        break;
                    case 2:
                         $nReaction->color = 'green';
                        break;
                    case 3:
                         $nReaction->color = 'yellow';
                        break;
                    default:
                      
                        break;
                }
               
                $nReaction->save();
            }            
            $nParticipant = new Participant();
            $id_participant = uniqid();
            $nParticipant->id_participant = $id_participant;
            $nParticipant->first_name = $value.rand();
            $nParticipant->email = $value.rand().'@youpi.fr';
            $nParticipant->last_name = strtoupper($value.rand());
            $nParticipant->password = password_hash($value."poivre42", PASSWORD_DEFAULT);
            $random = rand(0, 10);
            if($random <= 2 || $createur==0){
               $nParticipant->id_droit = 1 ; 
               $nExp->id_createur = $id_participant;
               $createur = $id_participant;
            }else{
                $nExp->id_createur = $createur;
            }

            $nParticipant->save();
            $ntag = new Tag();
            $ntag->id_tag = uniqid();
            $ntag->name = $value.'tag';
            $ntag->experiences()->attach($nExp->id_experience);
            $ntag->save();
            $nExp->save();
        }
        $allParticipants = Participant::all()->take(10);
        $allExp = Experience::all();
        foreach ($allExp as $key2 => $exp) {
            foreach ($allParticipants as $key => $particpant) {
                
                $reactionsExperience = $exp->reactions()->get();
                foreach ($reactionsExperience as $key3 => $reaction) {
                    for ($i=0; $i < 5; $i++) { 
                        $nRtp = new rtp();    
                        $nRtp->nom_reaction = $reaction->name;
                        $nRtp->id_participant = $particpant->id_participant;
                        $nRtp->time = rand(0, 290);
                        $nRtp->id_experience = $exp->id_experience;
                        $nRtp->id_reactiontoparticipant = uniqid();
                        $nRtp->save();
                    }
                }
                
            }
        }
		return $this->view->render($response, 'bienvenue.twig', $res);
    }

    public function viderBdd(Request $request, Response $response, $args){
        $res = [];
        Experience::truncate();
        Participant::truncate();
        rtp::truncate();
        Reaction::truncate();
        Comment::truncate();
        Tag::truncate();


        return $this->view->render($response, 'bienvenue.twig', $res);
    }
}