<?php

namespace App\Controllers;

use App\Models\Experience;
use App\Models\Video;
use App\Models\Reaction;
use App\Models\ReactionToParticipant as rtp;
use App\Models\Tag;

use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


final class SearchController extends Controller{
	public function search(Request $request, Response $response, $args){
		$res = [];
		if(!empty($_GET['motcle'])){
			$res["experiences"]= Experience::where('title', 'LIKE', '%'.$_GET['motcle'].'%')->get();
			$tags = Tag::where("name", "LIKE", '%'.$_GET['motcle'].'%')->get();
			$res["tags"] =$tags;
			$res["experiencesToTags"] = [];
			foreach ($tags as $key => $value) {
				$experiencesToTags = $value->experiences()->get();
				$res["experiencesToTags"] [$value->name] [] = $experiencesToTags; 
			}

		}

		$this->view->render($response, 'resultatSearch.twig', $res);
	}
}