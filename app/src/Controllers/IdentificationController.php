<?php

namespace App\Controllers;


use App\Models\Participant;

use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


class IdentificationController extends Controller{

	public function verifInscription(Request $request, Response $response){
		$res = [];
		$champs = false;
        $mdp = false;
        $mdp2 = false;
        $email = false;
        $email2 = true;
        if( !empty($_POST['mdp']) && !empty($_POST['mdpcheck']) &&  !empty($_POST['email']) && !empty($_POST['nom']) && !empty($_POST['prenom'])){ //on check que tout est rempli
            $champs = true;
        }else{
           $res ['error'][] = "veuillez remplir tous les champs obligatoire";
        }
        if(strlen($_POST["mdp"])>=6){
            $mdp = true;
        }else{
            $res ['error'][] =  "mot de passe trop court";
        }
        if($_POST["mdp"] == $_POST["mdpcheck"]){
            $mdp2 = true;
        }else{
           $res ['error'][] =  "les mots de passe doivent correspondre";
        }
        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            $email = true;
        }else{
            $res ['error'][] =  'adresse mail invalide' ;
        }

        $allPart = Participant::all();

        foreach ($allPart as $key => $user) {
            if($user->email == $_POST['email']){
                $email2=false;
            }
        }
        if ($email2 == false){
             $res ['error'][] = 'email déja utilisé ';
        }

        if($champs && $mdp && $mdp2 && $email && $email2){
            $newPart = new Participant();
            $id_participant = uniqid();
            $newPart->id_participant = $id_participant;
            $newPart->email = $_POST['email'];
            $newPart->first_name = $_POST['prenom'];
            $newPart->last_name = $_POST['nom'];
            $newPart->password = password_hash($_POST['mdp']."poivre42", PASSWORD_DEFAULT);
            $newPart->save();
            $value = $newPart;

          }

        if(empty($res)){
            return $response->withRedirect($this->router->pathFor('homepage'));
        }else{
            return $this->view->render($response, 'inscription.twig', $res);
        }

	}


	public function verifConnexion(Request $request, Response $response){
		$res;
		if(!empty($_POST['email'] && !empty($_POST['mdp']))){
        	$part = Participant::where('email', $_POST['email'])->get();
        	if(count($part) !=0){
           		foreach ($part as $key => $value) {
                	if(password_verify($_POST['mdp']."poivre42", $value->password)){
                   		$_SESSION["id_participant"] = $value->id_participant;
          			      $_SESSION["isConnected"] = true;
											$_SESSION["droit"] = $value->id_droit;
                	}else{
                    	$res['error'][] = "mauvais mot de passe";
                	}
            	}
        	}else{
            	$res['error'][] = "Email introuvable";
        	}
    	}
        if(!empty($res)){
            return $this->connexion($request, $response, $res);
        }return $response->withRedirect($this->router->pathFor('liste_experience'));

  	}



	public function connexion(Request $request, Response $response, $args){
		return $this->view->render($response, 'connexion.twig', $args);
	}

	public function inscription(Request $request, Response $response){
		return $this->view->render($response, 'inscription.twig');
	}

	public function deconnexion(Request $request, Response $response){
		session_unset();
		return $response->withRedirect($this->router->pathFor('homepage'));
        exit();
	}

	public function bienvenue(Request $request, Response $response, $args){
		return $this->view->render($response, 'bienvenue.twig', $args);
	}

}
