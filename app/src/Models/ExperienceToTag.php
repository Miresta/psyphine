<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class experiencetotag extends Model{
  	protected $table = "experiencetotag";
  	protected $primaryKey = 'id_tag';
  	public $timestamps=false;
  	public $incrementing = false;

  	public function experience(){
  		return $this->belongsTo('App\Models\Experience', 'id_experience');
  	}
}
