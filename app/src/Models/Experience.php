<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model{
  	protected $table = "experience";
  	protected $primaryKey = 'id_experience';
  	public $timestamps=true;
  	public $incrementing = false;
	
  	public function reactions(){
  		return $this->hasMany('App\Models\Reaction', 'id_experience');
  	}

    public function tags(){
      return $this->belongsToMany('App\Models\Tag', 'experiencetotag','id_experience', 'id_tag') ; 
    }

  	public function video(){
  		return $this->hasOne('App\Models\Video', 'id_video');
  	}
}