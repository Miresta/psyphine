var tabTimeLine;
var tabIsDefine = false;

document.addEventListener( "DOMContentLoaded", function() {

}, true);

function interaction(id){
	var ct = $("#jquery_jplayer_1").data("jPlayer").status.currentTime;
    var time = $('#jquery_jplayer_1').data("jPlayer").status.duration;
		var reac = id.split(' ');
	var color = reac[1];
    var nomReaction= reac[2];
    var id = reac[3];
    if(!tabIsDefine){
        creationTableau(time, '#tableauTimeline');
    }
    addReaction(color, ct, nomReaction, id);
	//addReaction(nomReaction, pc.currentTime());
}

function creationTableau(duration, containerid){
    var container = $(containerid);
    var nbCase = Math.round(duration)/5;

    var tbody = $('<tbody>');
    var nbBoutons = $('.reactionButton').length;
    for(var j=0; j< nbBoutons; j++){
        var ligne = $('<tr>').addClass('tr');
        var td;
        for (var i = 0 ; i<=nbCase; i++) {
            td = $('<td>').attr("id", i+"td"+j);
            ligne.append(td);
        }
        tbody.append(ligne);
    }

    container.append(tbody);
    tabIsDefine = true;
}

function addReaction(color, time, reaction, id){
    var idCase =  (Math.round(time/5))+"td"+id;
    let cas = $('#'+idCase);
    cas.mouseover(function(){
        $("#etiquette").empty();
        var minutes = Math.floor(time / 60);
        var seconds = time - minutes * 60;
        var s = $("<p>").text(reaction+" à "+minutes+":"+Math.round(seconds)+" s");
        $("#etiquette").append(s);

    });
    cas.addClass('caseReaction');
    cas.addClass(color);
    var input = $("#reactionsTextArea").val($("#reactionsTextArea").val()+Math.round(time)+" "+reaction+";");
    $.post( "ajax.php", {reaction : reaction}, function( data ) {
        console.log(data);
    });


}
