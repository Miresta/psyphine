<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReactionToParticipant extends Model{
  	protected $table = "reactiontoparticipant";
  	protected $primaryKey = 'id_reactiontoparticipant';
  	public $timestamps=false;
  	public $incrementing = false;

  	public function experience(){
  		return $this->belongsTo('App\Models\Experience', 'id_experience');
  	}
}