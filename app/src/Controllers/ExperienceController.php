<?php

namespace App\Controllers;

use App\Models\Experience;
use App\Models\Video;
use App\Models\Reaction;
use App\Models\Tag;
use App\Models\ExperienceToTag;
use App\Models\Comment;
use App\Models\Participant;

use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class ExperienceController extends Controller{

    public function experience(Request $request, Response $response, $args){
    	$res = [];
    	if(isset($_GET['experience'])){
    		$experience=Experience::find($_GET['experience']);
    		$res['experience'] = $experience;
    		$res['reactions']= $experience->reactions()->get();
            $v = Video::find($experience->id_video);
            $res['video']= $v;
    	}else{
    		$res['error'][] = 'aucune experience selectionné';
    	}
      $comments = Comment::where('id_experience', $_GET['experience'])->get();
            //              ->where('id_participant', $_SESSION['id_participant'])
              //  = Participant::where('id_participant')
      $experienceC = Experience::where('id_experience', $_GET['experience'])->get();
      $res['experienceC'] = $experienceC;
      $res['comments']= $comments;

      //$participant = Participant::where('id_participant', $comments->id_participant)->get();

    //  $res['participant']= $participant;


        return $this->view->render($response, 'experience.twig', $res);
    }

    public function formExperience(Request $request, Response $response, $args){
      if ($_SESSION["droit"] == 0){
        echo "vous n'êtes pas autorisé à accéder à ce contenu";
      }
      else{
    	$res = $args;
    	return $this->view->render($response, 'formExperience.twig', $res);
      }
    }

    public function liste_experience(Request $request, Response $response, $args){
      $res = [];
      $res["experiences"] = Experience::all();
      return $this->view->render($response, 'listeExperiences.twig', $res);
    }

    public function checkFormExperience(Request $request, Response $response, $args){
    	$res = $args;
        if(!empty($_POST['title'])){
            if( !empty($_POST['video']) || is_uploaded_file($_FILES['videoUpload']['tmp'])){
                $newExp = new Experience();
                $newExp->id_experience = uniqid();
                $newExp->id_createur = $_SESSION['id_participant'];
                $idvideo = uniqid();
                $newVideo = new Video();
                $newVideo->id_video = $idvideo ;
                $newVideo->id_experience = $newExp->id_experience;
                if(!empty($_POST['video'])){
                    $newVideo->url = $_POST['video'];
                    $newVideo->name = $_POST['video'];
                }elseif (isset($_FILES['videoUpload'])){
                    if (isset($_FILES['videoUpload']['error'])){
                        switch ($_FILES['videoUpload']['error']){
                            case 1: // UPLOAD_ERR_INI_SIZE
                            $res['error'] [] = "Le fichier dépasse la limite autorisée par le serveur!";
                            break;
                            case 2: // UPLOAD_ERR_FORM_SIZE
                            $res['error'] [] = "Le fichier dépasse la limite autorisée dans le form HTML !";
                            break;
                            case 3: // UPLOAD_ERR_PARTIAL
                            $res['error'] [] = "L'envoi du fichier a été interrompu pendant le transfert !";
                            break;
                            case 4: // UPLOAD_ERR_NO_FILE
                            $res['error'] [] = "Le fichier que vous avez envoyé a une taille nulle !";
                            break;
                        }
                    }else{
                        $infoFile =  pathinfo($_FILES['videoUpload']['name']);
                        $extension= $infoFile["extension"];
                        if($extension == 'avi' || $extension == 'mp4'){
                            $video = "video";
                            $pos = strpos($_FILES['videoUpload']['type'], $video);
                            if($pos !== false){
                                if(move_uploaded_file($_FILES['videoUpload']['tmp_name'], 'video/'.$newVideo->id_experience.'.'.$extension)){
                                    $newVideo->url = 'video'.$newVideo->id_experience.'.'.$extension;
                                    $newVideo->name = $_FILES['videoUpload']['name'];
                                    $newVideo->size = $_FILES['videoUpload']['size'];
                                }else{
                                    $res['error'] [] = "l'upload s'est mal passé";
                                }
                            }else{
                                $res['error'] [] = "ce n'est pas une video";
                            }
                        }else{
                            $res['error'] [] = "le format n'est pas valide";
                        }
                    }
                }
                $newVideo->save();
                $newExp->id_video = $idvideo;
                $newExp->title = $_POST['title'];
                $newExp->id_state = 1;
                if(!empty($_POST['presentation'])){
                    $newExp->presentation = $_POST['presentation'];
                }

                if(!empty($_POST['consignes'])){
                    $newExp->explication = $_POST['consignes'];
                }

                if(!empty($_POST['button'])){

                    foreach($_POST['button'] as $key_button => $value_button){
                    $value_button = str_replace(' ', '_', $value_button);
                      foreach($_POST['color'] as $key_color => $value_color){
                        if($key_button == $key_color){
                          $newReac = new Reaction();
                          $newReac->id_reaction = uniqid();
                          $newReac->id_experience = $newExp->id_experience;
                          $newReac->name = $value_button;
                          $newReac->color = $value_color;
                          $newReac->save();
                        }
                      }
                    }
                }


                if (!empty($_POST['tag'])){
                    foreach($_POST['tag'] as $key_tag => $value_tag){
                      $newTag = new Tag();
                      $newTag -> id_tag = uniqid();
                      $newTag -> name = $value_tag;
                      $newTag -> save();

                      $ExperienceToTag = new ExperienceToTag();
                      $ExperienceToTag -> id_tag = $newTag -> id_tag;
                      $ExperienceToTag -> id_experience = $newExp->id_experience;
                      $ExperienceToTag -> save();

                    }
                }


                $newExp->save();
                $_SESSION['experience']=$newExp;
            }else{
              $res['error'][] = "Merci d'indiquer une url ou d'uploader une vidéo";
            }
        }else{
            $res['error'][] = "Veuillez indiquer un titre à votre expérience";
        }
        //
        // var_dump($_POST);
        // $a = is_uploaded_file($_FILES['videoUpload']['tmp']);
        // var_dump($a);

        if(empty($res['error'])){
          $res['success'][] = "succès";
            return $this->view->render($response, 'formExperience.twig', $res);
        }else{
            return $this->view->render($response, 'formExperience.twig', $res);
        }
    }

      public function commentExperience($request, $response){
      	$res = [];
          if(isset($_GET['comment'])){
            $comments = Comment::where('id_experience', $_GET['experience'])
                        ->where('id_participant', $_SESSION['id_participant'])->get();
            $dejaPoste = false;
            foreach ($comments as $key => $value) {
                if($value->comment == $_GET['comment']){
                    $dejaPoste = true;
                }
            }
            if(!$dejaPoste){
                $comment = new Comment();
                $comment->id_comment = uniqid();
                $comment->id_participant = $_SESSION['id_participant'];
                $comment->id_experience = $_GET['experience'];
                $comment->comment = $_GET['comment'];
                $comment->size = strlen($_GET['comment']);
                /*if(isset(['notation'])){
              		$comment->notation = $_POST['notation'];
            	}*/
                $comment->save();

            }else{
                echo '<p>Vous avez déja posté ce commentaire</p>';
            }

    }else{
      echo '<p>Ajouté un commentaire</p>';
    }

  	return $this->experience($request, $response, $res);


	}

	public function deleteComment($request,$response){
    if(isset($_GET['comment'])){
        if(Comment::find($_GET['comment'])){
        $comment = Comment::find($_GET['comment'])->delete();
        }
    }

	return $this->experience($request, $response, []);
	}
}
