<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Video extends Model{
  	protected $table = "video";
  	protected $primaryKey = 'id_video';
  	public $timestamps=false;
  	public $incrementing = false;
	
  	public function experience(){
  		return $this->belongsTo('App\Models\Experience', 'id_experience');
  	}	

}