<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Experience;

class Comment extends Model{

    protected $table='comments';
    protected $primaryKey= 'id_comment';
    public $timestamps=true;
    public $incrementing = false;
}
