<?php
/*
	Routes
	controller needs to be registered in dependency.php
*/

// $app->get('/', 'App\Controllers\IdentificationController:connexion')->setName('homepage');
$app->get('/', 'App\Controllers\IdentificationController:bienvenue')->setName('homepage');
$app->get('/users', 'App\Controllers\UserController:dispatch')->setName('userpage');

$app->get('/experience', 'App\Controllers\ExperienceController:experience');
$app->get('/createExperience', 'App\Controllers\ExperienceController:formExperience')->setName('creation_exp');
$app->post('/checkForm', 'App\Controllers\ExperienceController:checkFormExperience');
$app->post('/ajouterInteraction', 'App\Controllers\InteractionController:ajouterReaction');
$app->get('/recherche', 'App\Controllers\SearchController:search');


$app->get('/bienvenue', 'App\Controllers\IdentificationController:bienvenue');
$app->get('/liste_experience', 'App\Controllers\ExperienceController:liste_experience')->setName('liste_experience');
$app->get('/definChercheur', 'App\Controllers\UserController:definChercheur')->setName('liste_participants');
$app->post('/updateDroit', 'App\Controllers\UserController:updateDroit');


//identificationController
$app->get('/inscription', 'App\Controllers\IdentificationController:inscription');

$app->post('/verifInscription', 'App\Controllers\IdentificationController:verifInscription');

$app->get('/connexion', 'App\Controllers\IdentificationController:connexion');

$app->post('/verifConnexion', 'App\Controllers\IdentificationController:verifConnexion');
$app->get('/deconexion', 'App\Controllers\IdentificationController:deconnexion');

$app->get('/bdd', 'App\Controllers\BddController:bdd');

$app->get('/viderBdd', 'App\Controllers\BddController:viderBdd');


$app->get('/profil', 'App\Controllers\UserController:profil');

$app->get('/ajouterCommentaire', 'App\Controllers\ExperienceController:commentExperience');
$app->get('/deleteComment', 'App\Controllers\ExperienceController:deleteComment');

