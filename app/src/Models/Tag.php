<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model{
  	protected $table = "tag";
  	protected $primaryKey = 'id_tag';
  	public $timestamps=false;
  	public $incrementing = false;

  	public function experiences(){
  		return $this->belongsToMany('App\Models\Experience', 'experiencetotag', 'id_tag', 'id_experience');
  	}
}