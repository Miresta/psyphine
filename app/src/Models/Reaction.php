<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reaction extends Model{
  	protected $table = "reaction";
  	protected $primaryKey = 'id_reaction';
  	public $timestamps=false;
  	public $incrementing = false;

  	public function experience(){
  		return $this->belongsTo('App\Models\Experience', 'id_experience');
  	}
}