<?php

namespace App\Controllers;

use App\Models\Experience;
use App\Models\Video;
use App\Models\Reaction;
use App\Models\ReactionToParticipant as rtp;

use Slim\Views\Twig as View;
use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


final class InteractionController extends Controller{

	public function ajouterReaction (Request $request, Response $response, $args){
		$res = [];
		$exp = Experience::find($_GET['experience']);
		if(!empty($_POST['reaction']) && !empty($_GET['experience'])){
			$tabReactions = explode(";", $_POST['reaction']);
			$participant = $_SESSION['id_participant'];
			$res["video"] = Video::find($exp->id_video);
			Video::where('id_video', $exp->id_video)->update(['size' => floor($_POST['duree'])]);
			foreach ($tabReactions as $key => $value) {
				if($value != ""){
					$time = explode(" ", $value)[0];
					$reaction = explode(" ", $value)[1];
					$rtp = new rtp();
					$rtp->nom_reaction = $reaction;
					$rtp->time = $time;
					$rtp->id_experience = $_GET['experience'];
					$rtp->id_reactiontoparticipant = uniqid();
					$rtp->id_participant = $participant;
					$rtp->save();
				}

			}
			$res["reactions"] = $exp->reactions()->get();
			$res['experience'] = $exp ;
			$res['participants'] = [];
			$res['AllReactionsUsers'] = [];
			foreach ($exp->reactions()->get() as $key => $value) {
				$reactionsAllUsers = rtp::all()->where('id_experience', $exp->id_experience)->where('nom_reaction', $value->name);
				$res['AllReactionsUsers'][$value->name] = $reactionsAllUsers;
				foreach ($reactionsAllUsers as $key => $value2) {
					if(!in_array($value2->id_participant, $res['participants'])){
						$res['participants'][] = $value2->id_participant;
					}
				}
			}
		}
		$this->view->render($response, 'resultatExperience.twig', $res);
	}


}
