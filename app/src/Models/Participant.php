<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model{
  	protected $table = "participant";
  	protected $primaryKey = 'id_participant';
  	public $timestamps=false;
  	public $incrementing = false;
}
