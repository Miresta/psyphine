var tabTimeLine;
var tabIsDefine = false;
var donnee;
var duree;

document.addEventListener( "DOMContentLoaded", function() {

}, true);

function interaction(id){
	duree = $('#jquery_jplayer_1').data("jPlayer").status.duration;
	var ct = $("#jquery_jplayer_1").data("jPlayer").status.currentTime;
	var color = id.split(' ')[1];
  var nomReaction= id.split(' ')[2];
  var id = id.split(' ')[3];
	var maxId = $('.reactionButton:last-child').prop('id')[0];
  if(!tabIsDefine){
		donnee = creerTableau(maxId);
  }
  addReaction(color, ct, nomReaction, id);
}


function addReaction(color, time, reaction, id, maxId){
	var minMax = duree/60;
	var secMax = duree % 60;
	var minsecMax = Math.trunc(minMax) + ':' + Math.trunc(secMax);
	console.log(minsecMax.toString());
	var options = {
		elements: {
				points: {
						borderWidth: 1,
						borderColor: 'rgb(0, 0, 0)'
				}
		},
		scales : {
			yAxes : [{
				ticks : {
					display : false,
					min : -1,
					max : 1,
					stepSize : 1
				},
				gridLines : {
					drawOnChartArea : false
				}
			}],
			xAxes : [{
			/*	type : 'time',
				time : {
					parser : 'String',
					displayFormats : {
						'minsec' : 'm:s'
					},
					unit : 'minsec'
				},
				ticks : {
					min : '0:0',
					max : minsecMax.toString()
				}*/
				ticks : {
					min : 0,
					max : Math.trunc(duree)
				}
			}]
		}
	};
		var minutes = time / 60;
		var seconds = time % 60;
		var temps = Math.trunc(minutes)+':'+Math.trunc(seconds);
		console.log(temps);
		//temps = new Date(time*1000);
		//temps = temps.getMinutes() + temps.getSeconds();
		//console.log(temps.getMinutes());
		donnee[id] = donnee[id].concat({x:time, y:0, r:10});
		console.log(donnee);
		var data={
			datasets: [
			{
				label: reaction,
				data: donnee[id],
				backgroundColor:color,
				hoverBackgroundColor: color
			}]
		};

		var ctx = document.getElementById("chart_"+reaction);
		var graphique = new Chart(ctx, {
		    type: 'bubble',
		    data: data,
		    options: options
		});

    var idCase =  (Math.round(time/5))+"td"+id;
    let cas = $('#'+idCase);
    cas.mouseover(function(){
        $("#etiquette").empty();
        var minutes = Math.floor(time / 60);
        var seconds = time - minutes * 60;
        var s = $("<p>").text(reaction+" à "+minutes+":"+Math.round(seconds)+" s");
        $("#etiquette").append(s);

    });
    cas.addClass('caseReaction');
    cas.addClass(color);
    var input = $("#reactionsTextArea").val($("#reactionsTextArea").val()+Math.round(time)+" "+reaction+";");


}
function creerTableau(maxId){
	$('#duree').append(duree);
	var donnee = new Array(maxId);
	for (var i = 0; i < maxId; i++) {
		donnee[i] = [];
	}
	tabIsDefine = true;
	return donnee;
}
