<?php

namespace App\Controllers;

use App\Models\Experience;
use App\Models\Video;
use App\Models\Reaction;
use App\Models\ReactionToParticipant as rtp;
use App\Models\Participant;
use App\Models\Droit;
use App\Models\Comment;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;


use Slim\Views\Twig as View;

class UserController extends Controller{

    // private $view;
    // private $logger;
    // private $user;
    //
    // public function __construct($view, LoggerInterface $logger, $user){
    //     $this->view = $view;
    //     $this->logger = $logger;
		// $this->model = $user;
    // }

    public function dispatch(Request $request, Response $response, $args){
        $this->logger->info("Home page action dispatched");
		$users = $this->model->show();
		return $this->view->render($response, 'users.twig', ["data" => $users]);
    }

    public function verifConnexion(Request $request, Response $response, $args){
        $res = [];
        $res["experiences"] = Experience::all();
        return $this->view->render($response, 'listeExperiences.twig', $res);
    }

    public function definChercheur(Request $request, Response $response, $args){
      $res = [];
      $res["participants"] = Participant::where('id_droit', 0)->get();
      return $this->view->render($response, 'listeParticipants.twig', $res);
    }

    public function updateDroit(Request $request, Response $response, $args){
      $res['success'][] = "succès";
      Participant::where('id_participant', $_POST["id_participant"])->update(['id_droit' => 1]);
      return $response->withRedirect($this->router->pathFor('liste_participants'));
      // return $this->view->render($response, 'listeParticipants.twig', $res);
    }


    public function profil(Request $request, Response $response, $args){
      $res = [];
      $status = Participant::where('id_participant', $_SESSION['id_participant'])->first();
      $droit = Droit::where('id_droit', $status->id_droit)->first();
      $res['droit'] = $droit->description;
      $res['utilisateur'] = Participant::where('id_participant', $_SESSION['id_participant'])->first();
      if($droit->id_droit == 1){
        $i = 0;
        $exps = Experience::where('id_createur', $_SESSION['id_participant'])->get();
  		 	$res['participants'][] = [];
  		 	$res['AllReactionsUsers'][] = [];
        $res['nbReaction'][] = [];
        $res['video'] = [];
        foreach ($exps as $exp) {
          $v = Video::where('id_video', $exp->id_video)->first();
          $res['video'][$i] = $v;
          $res['experiences'][$i] = $exp;
          $res['reactions'][$i] = $exp->reactions()->get();
          $res['nbReactions'][$i] = sizeof($res['reactions'][$i]);
          foreach ($exp->reactions()->get() as $value) {
    		 		$reactionsAllUsers = rtp::all()->where('id_experience', $exp->id_experience)->where('nom_reaction', $value->name);
    		 		$res['AllReactionsUsers'][$i][$value->name] = $reactionsAllUsers;
    		  }
          $res['participants'][$i] = rtp::distinct()->where('id_experience', $exp->id_experience)->get(['id_participant']);
          $i++;
        }
      }else{
        $participation = rtp::distinct()->select('id_experience')->where("id_participant", $_SESSION['id_participant'])->get();
        foreach ($participation as $exp) {
          $res['participations'][] = Experience::where('id_experience', $exp->id_experience)->get();
        }
      }
      return $this->view->render($response, 'profil.twig', $res);
    }


    public function commentExperience($request, $response){

    //echo $_POST["experience"];
    //var_dump($_POST);
          if(isset($_POST['comment'])){
            $comments = Comment::where('id_experience', $_POST['experience'])
                        ->where('id_participant', $_SESSION['id_participant'])->get();
            $dejaPoste = false;
            foreach ($comments as $key => $value) {
                if($value->comment == $_POST['comment']){
                    $dejaPoste = true;
                }
            }
            if(!$dejaPoste){
                $comment = new Comment();
                $comment->id_comment = uniqid();
                $comment->id_participant = $_SESSION['id_participant'];
                $comment->id_experience = $_POST['experience'];
                $comment->comment = $_POST['comment'];
                $comment->size = strlen($_POST['comment']);
                if(isset($_POST['notation'])){
              $comment->notation = $_POST['notation'];
            }
                $comment->save();

            }else{
                //echo '<p>Vous avez déja posté ce commentaire</p>';
            }

    }else{
      //echo '<p>Ajouté un commentaire</p>';
    }
  // header ('Location : experience?experience='$_GET['experience']);

  // header ('Location : /bienvenue');

     //header ('Location:experience?experience='.$_POST['experience'] );
    //  ob_clean();
    //  header ('Location:experience?experience='.$_POST['experience'] );
   $_SESSION['experience']=$_POST['experience'];
   return $this->detailExperience($request, $response);
    // exit;

}


public function detailExperience($request, $response){


     $experience = Experience::where('id_experience', $_POST['experience'])->get();
     $comments = Comment::where('id_experience', $_POST['experience'])->orderby('updated_at', 'DESC')->get();

    //
     //$comment = Comment::where('id_experience ', 'LIKE', '%'.$_GET['experience'].'%')->get();
     $comments = Comment::where('id_experience', $_POST['experience'])->get();
    //  ob_clean();
    //  header ('Location:experience?experience='.$_POST['experience'] );
//exit;
    return $this->view->render($response,'experience.twig', array('idUser' => $_SESSION['id_participant'], 'comments' => $comments, 'experience'=>$experience));//experienceD" => $detailExperience

}

public function deleteComment($request,$response){
    if(isset($_GET['comment'])){
        if(Comment::find($_GET['comment'])){
        $comment = Comment::find($_GET['comment'])->delete();
        }
    }
    $test=$_POST['experience'];
  //  header ('Location:experience?experience='.$test );
  ob_clean();
  header ('Location:experience?experience='.$_GET['experience'] );
//return $this->detailExperience($request, $response);
}
}
